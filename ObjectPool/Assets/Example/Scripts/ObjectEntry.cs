using System;
using UnityEngine;


[Serializable]
public class ObjectEntry
{
	public ObjectEntry()
	{
	}

	public GameObject prefab;	//Object you want to pool
	public int count;			//Amount of instances in pool
	public ObjectType type;		//Type of object

}

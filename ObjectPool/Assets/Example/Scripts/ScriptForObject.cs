﻿using UnityEngine;
using System.Collections;
using UGB.ObjPool;

public class ScriptForObject : MonoBehaviour {

	private GameObject mPool = null;
	private ObjectPoolExample mObjPool = null;
	private float mPutBackTimer = 3;
	private int mObjectType = 0;

	void Awake()
	{
		mPool = GameObject.FindGameObjectWithTag ("ObjPool");
		mObjPool = mPool.GetComponent<ObjectPoolExample> ();
		switch(gameObject.tag)
		{
		case "A":
			mObjectType = (int)ObjectType.ObjectA;
			break;
		case "B":
			mObjectType = (int)ObjectType.ObjectB;
			break;
		case "C":
			mObjectType = (int)ObjectType.ObjectC;
			break;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if(enabled)
		{
			if(mPutBackTimer > 0)
			{
				mPutBackTimer -= Time.deltaTime;
			}
			else
			{
				mPutBackTimer = 3;
				mObjPool.ReturnObject(this.gameObject, mObjectType, false); //type true to HideInHierarchy
			}
		}
	}
}

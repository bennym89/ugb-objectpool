﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UGB.ObjPool;

public class ObjectPoolExample : MonoBehaviour
{
	public List<ObjectEntry> mObjectsForPool = new List<ObjectEntry>();
	public ObjectPool mObjectPool = new ObjectPool();

	private GameObject mExampleObject = null;
	private GameObject mInstanceHolder = null;
	private GameObject mInactiveInstances = null;



	// Use this for initialization
	void Start () 
	{


		GameObject.DontDestroyOnLoad (this);

		mInstanceHolder = new GameObject ("Instances");
		mInactiveInstances = new GameObject ("Inactive");

		//Create one entry for each gameobject
		foreach(var ObjEntry in mObjectsForPool)
		{
			mObjectPool.CreateNewObjectPoolEntry(ObjEntry.prefab, (int)ObjEntry.type, ObjEntry.count);
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		#region Spawn Gameobject
		if(Input.GetKeyDown(KeyCode.Return))
		{
			int RandomObj = Random.Range(1,4);
			switch(RandomObj)
			{
			case 1:
				RandomObj = (int)ObjectType.ObjectA;
				break;
			case 2:
				RandomObj = (int)ObjectType.ObjectB;
				break;
			case 3:
				RandomObj = (int)ObjectType.ObjectC;
				break;
			default:
				break;
			}
			GetObject (RandomObj);
		}
		#endregion

		#region Print stats/information
		if(Input.GetKeyDown (KeyCode.F1))
		{
			mObjectPool.PrintStats ();
		}
		#endregion

		#region Reset stacks
		if(Input.GetKeyDown (KeyCode.D))
		{
			mObjectPool.ResetStacks();;
		}
		#endregion
	}

	private void GetObject(int pObjectType)
	{
		mExampleObject = mObjectPool.GetInstance (pObjectType);
		mExampleObject.transform.position = new Vector3 (0,2,0);
		mExampleObject.transform.parent = mInstanceHolder.transform;
	}

	public void ReturnObject(GameObject pObjectToPool, int pObjectType)
	{
		ReturnObject (pObjectToPool, pObjectType, true);
	}

	public void ReturnObject(GameObject pObjectToPool, int pObjectType, bool pHideInHierarchy = true)
	{
		pObjectToPool.transform.parent = mInactiveInstances.transform;
		mObjectPool.ReturnObject (pObjectToPool, pObjectType, pHideInHierarchy);
	}
}